<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register | Day 12 - Laravel</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <h3>Sign Up Form</h3>
    <form action="welcome" method="POST">
        @csrf
        <label>First Name:</label><br><br>
            <input type="text" name="first-name" placeholder="Insert first name"><br><br>
        <label>Last Name:</label><br><br>
            <input type="text" name="last-name" placeholder="Insert last name"><br><br>
        <label>Gender:</label><br><br>
            <input type="radio" name="gender" value="male">Male<br>
            <input type="radio" name="gender" value="female">Female<br>
            <input type="radio" name="gender" value="other">Other<br><br>
        <label>Nationality:</label><br><br>
            <select name="nationality">
                <option value="" disabled selected hidden>Insert nationality...</option>
                <option value="indonesia">Indonesian</option>
                <option value="us">American</option>
                <option value="uk">British</option>
                <option value="japan">Japanese</option>
                <option value="china">Chinese</option>
                <option value="india">Indian</option>
            </select><br><br>
        <label>Language Spoken:</label><br><br>
            <input type="checkbox" name="ls" value="indonesia">Bahasa Indonesia<br>
            <input type="checkbox" name="ls" value="english">English<br>
            <input type="checkbox" name="ls" value="other">Other<br><br>
        <label>Bio:</label><br><br>
            <textarea name="bio" cols="30" rows="10" placeholder="Insert bio here......"></textarea><br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>