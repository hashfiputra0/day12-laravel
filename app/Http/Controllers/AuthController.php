<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    // Return view register
    public function register()
    {
        return view('register');
    }

    // Return view welcome
    public function welcome(Request $request)
    {
        $data['name'] = $request["first-name"] . " " . $request["last-name"];
        return view('welcome', $data);
    }
}
